# shared
### network
+ 127.0.0.1:10000
+ 127.0.0.1:10001
### filesystem
+ /srv/data/mastodon
### dns
+ mastodon.d4.social
# setup
+ `certbot certonly --nginx -d d4.social -d mastodon.d4.social`
+ `sudo sysctl -w vm.max_map_count=262144` for elastic search
+ setup .wellkown redirect on d4.social
+ `ln -s /srv/truth/mastodon/nginx/mastodon.d4.social.conf /etc/nginx/sites-enabled/mastodon.d4.social && systemctl restart nginx`
+ `docker-compose run --rm web sh -c " SAFETY_ASSURED=1 DISABLE_DATABASE_ENVIRONMENT_CHECK=1 bundle exec rake db:setup"`
+ `docker-compose run web tootctl accounts create admin --email mastodon@d4rk.io --role admin --confirmed`
+ chown and set premissons on `/srv/data/mastodon/elasticsearch` user is uid 1000
